# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# append history file
shopt -s histappend

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# correct spelling mistakes when doing a cd
shopt -s cdspell

# Enable some Bash 4 features when possible:
# * `autocd`, e.g. `**/qux` will enter `./foo/bar/baz/qux`
# * Recursive globbing, e.g. `echo **/*.txt`
for option in autocd globstar; do
	shopt -s "$option" 2> /dev/null
done

# add user bin directory
PATH=./:$PATH
if [ -d ~/bin ]; then
	PATH=~/bin:$PATH
fi

if [ -f ~/.aliases ]; then
    source ~/.aliases
fi

# set a fancy prompt (non-color, overwrite the one in /etc/profile)
if [ -f ~/.ps1 ]; then
	source ~/.ps1
fi

if [ `which xinput` ]; then
	KBD_ID=$(xinput -list | grep -i typematrix | grep -i "slave.*keyboard" | awk -F'=' '{print $2}' | cut -c 1-2 | head -1 | tr -d ' ')
	if [ -n "$KBD_ID" ]; then
		setxkbmap -device $KBD_ID -layout dvorak
	fi
fi

if [ `which ponysay` ]; then
	ponysay "Hi !"
fi

function nix? {
	nix-env -qa \* -P --description | fgrep -i "$1";
}

ssh-add

export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'
export LESS_TERMCAP_ue=$'\E[0m'

export LESS="-FSRXIMj.2"

eval $(lesspipe)

export EDITOR="vim"

export VAGRANT_DEFAULT_PROVIDER="lxc"

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"

export GIMP2_DIRECTORY="$XDG_CONFIG_HOME"/gimp
export WEECHAT_HOME="$XDG_CONFIG_HOME"/weechat
export HTTPIE_CONFIG_DIR=$XDG_CONFIG_HOME/httpie
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export GTK_RC_FILES="$XDG_CONFIG_HOME"/gtk-1.0/gtkrc
export GNUPGHOME="$XDG_CONFIG_HOME/gnupg"
export VAGRANT_HOME="$XDG_CONFIG_HOME/vagrant"
export ATOM_HOME="$XDG_CONFIG_HOME"/atom

export LESSHISTFILE="$XDG_CACHE_HOME"/less/history
export DVDCSS_CACHE="$XDG_CACHE_HOME"/dvdcss

[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx
