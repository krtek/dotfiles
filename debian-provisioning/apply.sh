#!/usr/bin/env bash

ANSIBLE="ansible-playbook playbook.yml -c local"

if [ -z "$1" ]; then
    EXTRA=""
else
    ANSIBLE="$ANSIBLE --extra-vars '$1'"
fi

$ANSIBLE

