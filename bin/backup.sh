#!/bin/bash
REPOSITORY=/mnt/wd-backup/krtek-xps.borg

if [[ ! -d $REPOSITORY ]]; then
	mount $(dirname $REPOSITORY) &> /dev/null
	if [[ $? -ne 0 ]]; then
		WEEKDAY=$(date +%w)
		if [[ $WEEKDAY -ne 0 && $WEEKDAY -ne 6 ]]; then
			SCRIPT_PATH=$(cd `dirname $0`; pwd)
			SCRIPT_BIN=$SCRIPT_PATH/$(basename $0)

			export DISPLAY=:0
			export XAUTHORITY=/home/krtek/.Xauthority

			i3-nagbar -m "Unable to access $REPOSITORY" -b "Retry" "$SCRIPT_BIN" > /dev/null

			echo "Unable to mount backup drive"
			exit 1
		fi
		# exit without error on weekend day
		exit 0
	fi
fi

dpkg --get-selections > /root/dpkg_selections

# Backup all of /home and /var/www except a few
# excluded directories
borg create --stats          \
   $REPOSITORY::`date +%Y-%m-%d`               \
   /etc                                        \
   /home                                       \
   /opt                                        \
   /root                                       \
   --exclude *.pyc							\
   --exclude *.iso							\
   --exclude /home/*/Downloads				\
    --exclude /home/*/Documents/Roms         \
   --exclude /home/*/VirtualBox\ VMs		\
   --exclude /home/*/.local/share/Steam		\
   --exclude /home/*/.cache					\
   --exclude /home/*/.config/google-chrome	\
   --exclude /home/*/.config/vagrant    	\
   --exclude /home/krtek/Repositories/liip/ \
   --exclude /home/krtek/Temp

# Use the `prune` subcommand to maintain 7 daily, 4 weekly
# and 6 monthly archives.
attic prune $REPOSITORY --keep-daily=7 --keep-weekly=4 --keep-monthly=6
