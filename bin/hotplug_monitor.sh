#!/usr/bin/env bash

internal='eDP-1'

primary=(`xrandr --query | grep 'primary' | cut -f 1 -d ' '`)
externals=(`xrandr --query | grep ' connected' | grep -v $internal | cut -f 1 -d ' '`)
disconnected=(`xrandr --query | grep ' disconnected' | grep -v $internal | cut -f 1 -d ' '`)

for monitor in ${disconnected[@]}; do
    xrandr --output $monitor --off
done

if (( ${#externals[@]} > 0 )); then
	last=$internal
	for (( idx=${#externals[@]}-1 ; idx>=0 ; idx-- )) ; do
		external=${externals[idx]}
		xrandr --output $internal --auto --output $external --primary --auto --right-of $last
		last=$external
	done
else
   if (( $internal != $primary )); then
       xrandr --output $internal --auto --primary --output $primary --off
   fi
fi
