#!/usr/bin/env bash

DOTFILES=$( cd $(dirname "${BASH_SOURCE[0]}") && pwd)
WITH_ROOT=false

while getopts ire flag; do
	case $flag in
		r)
			WITH_ROOT=true
			;;
	esac
done

function link_file {
	SRC=$1
	DEST=$2

	if $TEST -d $SRC ; then
		if $TEST -L $DEST ; then
			$RM $DEST
			$LN $SRC $DEST
		elif $TEST -d $DEST ; then
			for FILE in $SRC/*; do
				OLD_DEST=$DEST
				link_file $FILE $DEST/$(basename $FILE)
				DEST=$OLD_DEST
			done
		else
			$LN $SRC $DEST
		fi
	elif $TEST -f $SRC ; then
		$RM $DEST
		$LN $SRC $DEST
	fi
}

function install_dir {
	if [[ $3 = true ]] ; then
		LN="sudo ln -s"
		RM="sudo rm -f"
		TEST="sudo test"
	else
		LN="ln -s"
		RM="rm -f"
		TEST="test"
	fi

	echo "Linking config file"
	for SRC in $DOTFILES/$1/*; do
		DST=$2/.$(basename $SRC)
		link_file $SRC $DST
	done

}

function install_homedir {
	install_dir homedir $1 $2

	echo "Configuring prompt"
	$RM $1/.ps1 $1/.ps1-root
	$LN $DOTFILES/liquidprompt/liquidprompt $1/.ps1

	if $TEST $1 != "/root" ; then
		echo "Adding ssh public keys."
		for rsa_pub in $DOTFILES/pubkeys/*; do
			cat $rsa_pub >> $1/.ssh/authorized_keys
		done
		echo " Done. DO NOT FORGET TO CLEAN THE KEYS IF NEEDED"
	fi

}

install_homedir ~ false

if [ $WITH_ROOT = true ]; then
	install_homedir /root true
fi
